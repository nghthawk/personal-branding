<nav class="navbar navbar-expand-lg navbar-light py-4">
  <div class="container">
      <a class="navbar-brand" href="/">
        <img src="./assets/images/logo.svg" alt="Logo" width="40" height="40" class="d-inline-block align-text-top">
      </a>
    <button class="navbar-toggler border-0 rounded-circle bg-purple text-white p-0 shadow" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span><i data-feather="menu"></i></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link" aria-current="page" href="/about">Über mich</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" aria-current="page" href="/#work">Arbeiten</a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="javascript:void(0)" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            Leistungen
          </a>
          <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
            <li><a class="dropdown-item" href="/staticPages">Statische Webseiten</a></li>
            <li><a class="dropdown-item" href="/wordpress">WordPress</a></li>
            <li><a class="dropdown-item" href="/printdesign">Print- & Mediendesign</a></li>
          </ul>
        </li>
        <li class="nav-item me-4">
          <a class="nav-link" aria-current="page" href="/faq">FAQ</a>
        </li>
      </ul>
      <a href="/#contact"><button class="btn-bg-purple">Kontakt</button></a>
    </div>
  </div>
</nav>
