<footer class="py-3">
  <div class="row container mx-auto px-0 py-5">
    <div class="col-6 col-lg-3">
      <ul class="list-unstyled">
        <li><b>David</b></li>
        <li><a href="/about">Über mich</a></li>
        <li><a href="/#work">Referenzen</a></li>
      </ul>
    </div>
    <div class="col-6 col-lg-3">
      <ul class="list-unstyled">
        <li><b>Leistungen</b></li>
        <li><a href="/staticPages">Statische Webseiten</a></li>
        <li><a href="/wordpress">WordPress</a></li>
        <li><a href="/printdesign">Print- & Mediendesign</a></li>
      </ul>
    </div>
    <div class="col-6 col-lg-3">
      <ul class="list-unstyled">
        <li><b>Nützliche Links</b></li>
        <li><a href="https://wordpress.org">wordpress.org</a></li>
        <li><a href="https://discord.com">discord.com</a></li>
        <li><a href="https://cloud.davidleven.de">ownCloud</a></li>
      </ul>
    </div>
    <div class="col-6 col-lg-3">
      <ul class="list-unstyled">
        <li><b>Rechtliches</b></li>
        <li><a href="/imprint">Impressum</a></li>
        <li><a href="/ds">Datenschutz</a></li>
        <li><a href="/#contact">Kontakt</a></li>
      </ul>
    </div>
    <div class="col-6 col-lg-3">
      <ul class="list-unstyled">
        <li><b>Social Media</b></li>
        <li><a href="#"><i data-feather="instagram"></i></a></li>
      </ul>
    </div>
    <div class="col-6 col-lg-3">
      <ul class="list-unstyled">
        <li><b>Sonstiges</b></li>
        <li><a href="/faq">FAQ</a></li>
        <li><a href="/nachhilfe">Schülernachhilfe</a></li>
      </ul>
    </div>
  </div>
  <div class="pre-footer text-center">
    <small>&copy; 2021 David |  <a data-bs-toggle="offcanvas" href="/imprint" role="button" aria-controls="offcanvasExample">Impressum</a> und <a href="/ds">Datenschutz</a> 🙂 </small>
  </div>
</footer>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous"></script>
<script>
  feather.replace();
  var popoverTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="popover"]'))
  var popoverList = popoverTriggerList.map(function (popoverTriggerEl) {
    return new bootstrap.Popover(popoverTriggerEl)
  })
</script>
