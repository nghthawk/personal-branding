<div class="pre_header text-white py-1">
    <div class="container d-flex justify-content-between">
      <small class="mobile_display_none">David Leven Web- & Grafikdesign</small>
      <div class="pre_header_contact">
        <i data-feather="phone" class="me-1"></i>
        <a href="mailto:info@davidleven.de" class="text-white"><small>0 1525 1799 246</small></a>
        <i data-feather="mail" class="ms-4 me-1"></i>
        <a href="mailto:info@davidleven.de" class="text-white"><small>info@davidleven.de</small></a>
      </div>
    </div>
  </div>
