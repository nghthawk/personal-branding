<!doctype html>
<html lang="en">
  <head>
    <!-- meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- css -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    <link href="css/main.css" rel="stylesheet" type="text/css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;700;800;900&display=swap" rel="stylesheet">
    <script src="https://unpkg.com/feather-icons"></script>
    <title>Wartungsarbeiten | davidleven.de</title>
    <script src="https://cdn.jsdelivr.net/npm/masonry-layout@4.2.2/dist/masonry.pkgd.min.js" integrity="sha384-GNFwBvfVxBkLMJpYMOABq3c+d3KnQxudP/mGPkzpZSTYykLBNsZEnG2D9G/X/+7D" crossorigin="anonymous" async></script>
  </head>
  <body>
    <main id="sub_service_page">
      <?php include "../site_elements/pre_header.php"; ?>
      <header id="with_background_subPage">
        <?php include "../site_elements/navigation.php"; ?>
        <div class="container row mx-auto pb-5">
          <h1 class="text-center mb-3"><span class="colored">Print-</span> und <span class="colored">Mediendesign</span></h1>
          <p class="text-center mx-auto mb-3 col-lg-5">Social Media Posts, Plakate, Visitenkarten und mehr - alles, was für einen starken Auftritt dazugehört.</p>
        </div>
      </header>
      <section class="primary_content container mt-5 mx-auto p-0 row">
        <aside class="col-lg-3">
          <div class="aside_header py-2 rounded mb-3">
            <b class="text-white text-center d-block mb-0">Übersicht</b>
          </div>
          <ul class="list-group list-group-flush">
            <li class="list-group-item"><a class="text-reset" href="#introduction">Was biete ich an?</a></li>
            <li class="list-group-item"><a class="text-reset" href="#pricing">Kosten</a></li>
            <li class="list-group-item"><a class="text-reset" href="#progress">Ablauf</a></li>
          </ul>
        </aside>
        <div class="col-lg-9 ps-5">
          <h2 class="mb-3 colored">Zielgruppe</h2>
          <div class="row gap-4 mb-5 p-2" id="customers">
            <div class="col-lg-4 rounded d-flex align-items-center justify-content-center text-center bg-success bg-opacity-10 py-4">
              <b>🏡<br>Örtliche Unternehmen</b>
            </div>
            <div class="col-lg-4 rounded d-flex align-items-center justify-content-center text-center bg-danger bg-opacity-10 py-4">
              <b>✍️<br>Selbstständige</b>
            </div>
            <div class="col-lg-4 rounded d-flex align-items-center justify-content-center text-center bg-info bg-opacity-10 py-4">
              <b>👨‍💻<br>Dienstleister</b>
            </div>
            <div class="col-lg-4 rounded d-flex align-items-center justify-content-center text-center bg-warning bg-opacity-10 py-4">
              <b>✒️<br>Blogger</b>
            </div>
          </div>
          <h2 id="#introduction" class="colored">Was biete ich an?</h2>
          <div class="row">
            <div class="col-1">
              <i data-feather="check" class="text-success"></i>
            </div>
            <div class="col-11">
              Visitenkarten, Schreibblöcke, Rechnungsformulare oder Briefpapier
            </div>
            <div class="col-1">
              <i data-feather="check" class="text-success"></i>
            </div>
            <div class="col-11">
              Social Media Designs: Angepasste Posts für Facebook, Instagram oder Twitter
            </div>
          </div>
          <h2 id="pricing" class="mt-5 colored">Anfallende Kosten</h2>
          <p>Im Vordergrund steht bei einer statischen Webseite das Erarbeiten des Designs. Es soll auf der einen Seite ansprechend sowie überzeugend, auf der anderen Seite aber auch zielführend und wirksam sein. In diesen Prozess fließt die meiste Arbeit, denn er setzt eine ausführliche Analyse und Recherche über die Zielgruppe deiner zukünftigen Webseite voraus.</p>
          <p>Die Entwicklung einer statischen Webseite startet deshalb <b>ab einem Preis von 400€</b>. Eine Webseite in diesem Preisbereich umfasst in der Regel eine Startseite und drei weitere Unterseiten nach Wahl (zum Beispiel Über mich, Portfolio und Impressum / Datenschutz).</p>
          <p>Wenn du bisher keinen <a href="#">Webspace</a> und keine <a href="#">Domain</a> hast, kümmere ich mich auf Wunsch darum. Die Kosten belaufen sich dabei auf <b>durchschnittlich 100€ pro Jahr</b>, variieren aber je nach Projektgröße.</p>
          <p><i data-feather="chevron-right"></i> Insgesamt solltest du also für eine statische Webseite ein Budget von mindestens <b>400€</b> und zusätzlich <b>100€ / Jahr für das Hosting</b> einplanen. Nachträgliche Änderungen am Design oder Inhalt berechne ich nach einem Stundensatz von <b>19€ pro angefangene Stunde</b>.</p>
          <h2 id="progress" class="mt-5 mb-4 colored">Ablauf</h2>
          <div class="row">
            <div class="col-lg-4">
              <h3>1. Erster Kontakt</h3>
              <p>Du nimmst mit mir Kontakt auf und teilst mir deine ersten Ideen oder Vorgaben für deine neue Webseite mit. Wenn du noch keine genauen Vorstellungen hast, erarbeiten wir diese gemeinsam - telefonisch, per Mail, in einem Videogespräch oder persönlich bei einer Tasse Café.</p>
            </div>
            <div class="col-lg-4">
              <h3>2. Recherche und Entwürfe</h3>
              <p>Ich informiere mich über die Zielgruppe deiner Webseite, setze mich mit deiner Kongurrenz auseinander und entwickle auf dieser Basis mögliche Entwürfe für deine Webseite. Das Ganze passiert transparent und orientiert an deinem Feedback.</p>
            </div>
            <div class="col-lg-4">
              <h3>3. Entwicklung</h3>
              <p>Wenn der finale Designentwurf steht, besprechen wir mögliche Änderungen oder Ergänzungen. Anschließend setze ich mich an die finale Umsetzung und schalte die Webseite letztendlich nach Rücksprache mit dir live. 🎈</p>
            </div>
          </div>
          <h2 id="conclusion" class="mt-5 colored">Zusammenfassung: Was bekommst du von mir?</h2>
          <div class="row mb-5">
            <div class="col-1">
              <i data-feather="arrow-right"></i>
            </div>
            <div class="col-11">
              Eine durchdachte, zielgerichtete und <a href="#">responsive</a> Webseite mit mindestens drei Unterseiten, welche zu dir und deinem Business passt.
            </div>
            <div class="col-1">
              <i data-feather="arrow-right"></i>
            </div>
            <div class="col-11">
              Einarbeitung sämtlicher Inhalte (Texte, Bilder, Videos, ...) in deine Webseite. Den gesamten technischen Aufwand übernehme ich.
            </div>
            <div class="col-1">
              <i data-feather="arrow-right"></i>
            </div>
            <div class="col-11">
              Dein individuelles Erscheinungsbild für dich und dein Business.
            </div>
          </div>
        </div>
      </section>
    </main>
    <section class="pre_footer container mx-auto bg-purple p-5 rounded row">
      <div class="col-md-6">
        <h3 class="pre_headline text-white">Kontakt</h3>
        <h2 class="text-white mb-3 lh-base">Überzeugt?<br>Abschließende Fragen?</h2>
      </div>
      <div class="col-md-6">
        <p class="text-white">Meld dich bei mir, damit wir <b>unverbindlich und kostenfrei</b> den ersten Schritt für deine neue Webseite gehen können.</p>
        <a href="#"><button class="btn-bg">Kontakt</button></a>
      </div>
    </section>
    <?php include "../site_elements/footer.php"; ?>
  </body>
</html>
