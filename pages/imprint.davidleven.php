<!doctype html>
<html lang="en">
  <head>
    <!-- meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- css -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    <link href="css/main.css" rel="stylesheet" type="text/css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;700;800;900&display=swap" rel="stylesheet">
    <script src="https://unpkg.com/feather-icons"></script>
    <title>Wartungsarbeiten | davidleven.de</title>
    <script src="https://cdn.jsdelivr.net/npm/masonry-layout@4.2.2/dist/masonry.pkgd.min.js" integrity="sha384-GNFwBvfVxBkLMJpYMOABq3c+d3KnQxudP/mGPkzpZSTYykLBNsZEnG2D9G/X/+7D" crossorigin="anonymous" async></script>
  </head>
  <body>
    <main>
      <?php include "../site_elements/pre_header.php"; ?>
      <header>
        <?php include "../site_elements/navigation.php"; ?>
      </header>
      <section class="container pb-5">
        <h1 class="mb-5 text-center">Impressum 🧐</h1>
        <p>
          <b>Verantwortlich nach §5 TMG:</b><br>
          David Leven<br>Wieser Weg 18<br>58809 Neuenrade<br>Deutschland
        </p>
        <p>
          <b>Kontakt:</b><br>
          info@davidleven.de<br>015251799246<br>Discord: Nighty#8248
        </p>
      </section>
    </main>
    <?php include "../site_elements/footer.php"; ?>
  </body>
</html>
