<!doctype html>
<html lang="en">
  <head>
    <!-- meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- css -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    <link href="css/main.css" rel="stylesheet" type="text/css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;700;800;900&display=swap" rel="stylesheet">
    <script src="https://unpkg.com/feather-icons"></script>
    <title>Wartungsarbeiten | davidleven.de</title>
    <script src="https://cdn.jsdelivr.net/npm/masonry-layout@4.2.2/dist/masonry.pkgd.min.js" integrity="sha384-GNFwBvfVxBkLMJpYMOABq3c+d3KnQxudP/mGPkzpZSTYykLBNsZEnG2D9G/X/+7D" crossorigin="anonymous" async></script>
  </head>
  <body>
    <main id="sub_service_page">
      <?php include "../site_elements/pre_header.php"; ?>
      <header id="with_background_subPage">
        <?php include "../site_elements/navigation.php"; ?>
        <div class="container row mx-auto pb-5">
          <h1 class="text-center mb-3">Webseite auf <span class="colored">WordPress</span> Basis</h1>
          <p class="text-center mx-auto mb-3 col-lg-5">Dieses Angebot richtet sich insbesondere an Blogger oder Personen, welche regelmäßig selbst Änderungen an ihrer Seite vornehmen möchten.</p>
        </div>
      </header>
      <section class="primary_content container mt-5 mx-auto p-0 row">
        <aside class="col-lg-3">
          <div class="aside_header py-2 rounded mb-3">
            <b class="text-white text-center d-block mb-0">Übersicht</b>
          </div>
          <ul class="list-group list-group-flush">
            <li class="list-group-item"><a class="text-reset" href="#introduction">Was genau ist WordPress?</a></li>
            <li class="list-group-item"><a class="text-reset" href="#whyWordPress">Warum WordPress?</a></li>
            <li class="list-group-item"><a class="text-reset" href="#pricing">Kosten</a></li>
            <li class="list-group-item"><a class="text-reset" href="#progress">Ablauf</a></li>
            <li class="list-group-item"><a class="text-reset" href="#outro">wordpress.org vs. wordpress.com</a></li>
            <li class="list-group-item"><a class="text-reset" href="#conclusion">Zusammenfassung: Was bekommst du?</a></li>
          </ul>
        </aside>
        <div class="col-lg-9 ps-5">
          <h2 class="mb-3 colored">Zielgruppen</h2>
          <div class="row gap-4 mb-5 p-2" id="customers">
            <div class="col-lg-4 rounded d-flex align-items-center justify-content-center text-center bg-warning bg-opacity-10 py-4">
              <b>✒️<br>Blogs</b>
            </div>
            <div class="col-lg-4 rounded d-flex align-items-center justify-content-center text-center bg-success bg-opacity-10 py-4">
              <b>✍️<br>Selbstständige Bearbeitung</b>
            </div>
            <div class="col-lg-4 rounded d-flex align-items-center justify-content-center text-center bg-info bg-opacity-10 py-4">
              <b>🔁<br>Regelmäßige Änderungen</b>
            </div>
            <div class="col-lg-4 rounded d-flex align-items-center justify-content-center text-center bg-danger bg-opacity-10 py-4">
              <b>🔌<br>Dynamisch</b>
            </div>
          </div>
          <h2 id="#introduction" class="colored">Was genau ist WordPress?</h2>
          <p>Bei WordPress handelt es sich um sogenanntes Content-Management-System. Es erlaubt dir also, selbst die Inhalte deiner Webseite zu erstellen und verwalten. Mittlerweile basieren rund 30% aller Webseiten im Internet nach eigenen Angaben auf WordPress. Der entscheidene Vorteil von WordPress ist die anfängerfreundliche Verwaltungsoberfläche, die von Jedermann ohne große Vorkenntnisse bedient werden kann.</p>
          <h2 id="whyWordPress" class="mt-5 colored">Warum WordPress?</h2>
          <div class="row">
            <div class="col-1">
              <i data-feather="check" class="text-success"></i>
            </div>
            <div class="col-11">
              Du bist in der Lage, sämtliche Inhalte auf der Webseite schnell und einfach selbst zu bearbeiten
            </div>
            <div class="col-1">
              <i data-feather="check" class="text-success"></i>
            </div>
            <div class="col-11">
              Zahlreiche öffentliche & kostenlose Plugins können den Funktionsumfang deiner Webseite erweitern (bspw. durch ein Kontaktformular, Google Analytics, ...)
            </div>
            <div class="col-1">
              <i data-feather="check" class="text-success"></i>
            </div>
            <div class="col-11">
              WordPress selbst ist kostenlos und es ist keine zusätzliche Software notwendig
            </div>
          </div>
          <h2 id="pricing" class="mt-5 colored">Anfallende Kosten</h2>
          <p>WordPress bietet zahlreiche kosenlose als auch kostenpflichtige Themes an, welche für den eigenen Gebrauch benutzt werden können. Einfach ausgedrückt verändert ein Theme das Design deiner zukünftigen Webseite. Da die Software selbst kostenfrei ist, beschränken sich eventuelle Kosten hierbei auf das Theme, welches für deine Webseite genutzt werden soll.<br><br>Da WordPress eigenständig gehostet wird, fallen dafür grundsätzlich ebenfalls Gebühren an. Wenn du bisher keinen <a href="#">Webspace</a> und keine <a href="#">Domain</a> hast, kümmere ich mich auf Wunsch darum. Die Kosten belaufen sich dabei auf <b>durchschnittlich 130€ pro Jahr</b>, variieren aber je nach Projektgröße.</p>
          <p>Neben der Auswahl eines geeigneten Themes und der Installation auf deinem Webspace übernehme ich ebenfalls die vollständige Einrichtung, sodass du nach Fertigstellung direkt losstarten kannst. Der genaue Preis ist abhängig von der Größe deiner Webseite und dem Theme, welches auf deine Zwecke angepasst werden muss. In der Regel nehme ich hierfür zwischen <b>550-750€</b>.</p>
          <p>Nach der Einrichtung erkläre ich dir, wie du deine Webseite anschließend eigenständig verwalten kannst und bespreche bestehende Fragen mit dir. Solltest du nach Fertigstellung weiterhin meine Unterstützung benötigen, berechne ich den Aufwand nach Stundenbasis, wobei ich <b>19€ pro angefangene Stunde</b> nehme.</p>
          <p><i data-feather="chevron-right"></i> Insgesamt solltest du für eine Webseite auf WordPress Basis also ein Budget von mindestens <b>550€</b> und zusätzlich <b>130€ / Jahr für das Hosting</b> einplanen.</p>
          <h2 id="progress" class="mt-5 mb-4 colored">Ablauf</h2>
          <div class="row">
            <div class="col-lg-4">
              <h3>1. Erster Kontakt</h3>
              <p>Du nimmst mit mir Kontakt auf und teilst mir deine ersten Ideen oder Vorgaben für deine neue WordPress-Webseite mit. Wenn du noch keine genauen Vorstellungen hast, erarbeiten wir diese gemeinsam - telefonisch, per Mail, in einem Videogespräch oder persönlich bei einer Tasse Café.</p>
            </div>
            <div class="col-lg-4">
              <h3>2. Themeauswahl & Einrichtung</h3>
              <p>Anschließend erstelle ich eine Auswahl an möglichen Themes, welche für deine Webseite in Frage kommen könnten. Wenn wir uns für ein Theme entschieden haben, kümmere ich mich vollständig um die Installation und Anpassung auf deine Bedürfnisse. Dabei informiere ich dich regelmäßig über die Fortschritte.</p>
            </div>
            <div class="col-lg-4">
              <h3>3. Fertigstellung</h3>
              <p>Abschließend erkläre ich dir die Benutzeroberfläche von WordPress, damit auch du in der Lage sein wirst, deine Webseite selbstständig zu erarbeiten. Natürlich stehe ich dir dabei bei anfallenden Fragen und Problemen zur Seite.</p>
            </div>
          </div>
          <h2 id="outro" class="mt-5 mb-4 colored">wordpress.org vs. wordpress.com: Wo liegt der Unterschied?</h2>
          <p>Im Internet liest man neben dem Begriff "WordPress" auch oft von wordpress.com. Hier ein kleiner Überblick, worin genau die Unterschiede liegen:</p>
          <div class="row">
            <div class="col-lg-6">
              <h3>wordpress.com</h3>
              <p>WordPress.com bezeichnet den Website-Baukasten der Firma Automattic, welcher auf der zuvor erwähnten Software WordPress basiert. Der Vorteil hierbei liegt darin, dass der Start in die WordPress-Welt schnell und einfach ist. Möchte man seine Webseite allerdings anpassen oder mit zusätzlichen Funktionen ausstatten, so stößt man schnell an Grenzen oder muss sich mit teuren Abonnements abfinden.</p>
            </div>
            <div class="col-lg-6">
              <h3>wordpress.org</h3>
              <p>Bei WordPress.org handelt es sich um die Software selbst. <a href="#pricing">Wie in der Kostenübersicht</a> bereits erwähnt, muss diese selbst gehostet werden. Dies erlaubt es uns, unsere Seite frei anzupassen und durch Plugins zu erweitern.</p>
            </div>
          </div>
          <h2 id="conclusion" class="mt-5 colored">Zusammenfassung: Was bekommst du von mir?</h2>
          <div class="row mb-5">
            <div class="col-1">
              <i data-feather="arrow-right"></i>
            </div>
            <div class="col-11">
              Eine durchdachte, zielgerichtete und <a href="#">responsive</a> Webseite, welche zu dir und deinem Business passt. Das verwendete Theme wird voll und ganz auf deine Bedürfnisse angepasst.
            </div>
            <div class="col-1">
              <i data-feather="arrow-right"></i>
            </div>
            <div class="col-11">
              Einarbeitung sämtlicher Inhalte (Texte, Bilder, Videos, ...) in deine Webseite. Den gesamten technischen Aufwand übernehme ich.
            </div>
            <div class="col-1">
              <i data-feather="arrow-right"></i>
            </div>
            <div class="col-11">
              WordPress als Content-Management-System. Zusätzliches Wissen, damit du deine Webseite in Zukunft selbstständig bearbeiten kannst. Zur Übersicht erhältst du ein PDF-Dokument mit allen relevanten Informationen von mir.
            </div>
          </div>
        </div>
      </section>
    </main>
    <section class="pre_footer container mx-auto bg-purple p-5 rounded row">
      <div class="col-md-6">
        <h3 class="pre_headline text-white">Kontakt</h3>
        <h2 class="text-white mb-3 lh-base">Überzeugt?<br>Abschließende Fragen?</h2>
      </div>
      <div class="col-md-6">
        <p class="text-white">Meld dich bei mir, damit wir <b>unverbindlich und kostenfrei</b> den ersten Schritt für deine neue Webseite gehen können.</p>
        <a href="#"><button class="btn-bg">Kontakt</button></a>
      </div>
    </section>
    <?php include "../site_elements/footer.php"; ?>
  </body>
</html>
