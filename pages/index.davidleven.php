<!doctype html>
<html lang="en">
  <head>
    <!-- meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- css -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    <link href="css/main.css" rel="stylesheet" type="text/css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;700;800;900&display=swap" rel="stylesheet">
    <script src="https://unpkg.com/feather-icons"></script>
    <title>Wartungsarbeiten | davidleven.de</title>
    <script src="https://cdn.jsdelivr.net/npm/masonry-layout@4.2.2/dist/masonry.pkgd.min.js" integrity="sha384-GNFwBvfVxBkLMJpYMOABq3c+d3KnQxudP/mGPkzpZSTYykLBNsZEnG2D9G/X/+7D" crossorigin="anonymous" async></script>
  </head>
  <body>
    <main id="index">
      <?php include "../site_elements/pre_header.php"; ?>
      <header id="with_background">
        <?php include "../site_elements/navigation.php"; ?>
        <div class="container row mx-auto px-0 py-5">
          <div class="col-lg-6 mb-5 pb-5">
            <h3 class="pre_headline">Hey, ich bin David. 🤘</h3>
            <h1 class="mb-4">Bereit, mit deiner eigenen Webseite durchzustarten? 🚀</h1>
            <p class="mb-4">Lorem ipsum dolor sit amet, consectet in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
            <a href="#work"><button class="btn-border me-3">Meine Arbeiten</button></a>
            <a href="#contact"><button class="btn-bg">Kontakt</button></a>
          </div>
          <div class="col-lg-4">

          </div>
        </div>
      </header>
      <section class="services my-5 py-5">
        <div class="container py-5 row justify-content-between mx-auto p-0">
          <div class="col-lg-3">
            <h3 class="pre_headline" id="services">Meine Leistungen</h3>
            <h2 class="mb-4 col-lg-9 lh-base">Wobei kann ich dir helfen?</h2>
          </div>
          <div class="col-lg-9 p-0">
            <div class="row g-0 p-0">
              <div class="col-md-6 col-lg-4 p-2 pt-0 ps-0">
                <div class="service_wrapper pt-0 p-3 rounded-3">
                  <div class="service_wrapper_icon mb-4 d-inline-block rounded-circle d-flex align-items-center justify-content-center text-white">
                    <i data-feather="type"></i>
                  </div>
                  <h3 class="mb-3">WordPress</h3>
                  <p>Lorem ipsum dolor sit ame nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor </p>
                </div>
              </div>
              <div class="col-md-6 col-lg-4 p-2 pt-0 ps-0">
                <div class="service_wrapper pt-0 p-3 rounded-3">
                  <div class="service_wrapper_icon mb-4 d-inline-block rounded-circle d-flex align-items-center justify-content-center text-white">
                    <i data-feather="layout"></i>
                  </div>
                  <h3 class="mb-3">Statische Webseiten</h3>
                  <p>Lorem ipsum dolor sit ame nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor </p>
                </div>
              </div>
              <div class="col-md-6 col-lg-4 p-2 pt-0 ps-0">
                <div class="service_wrapper pt-0 p-3 rounded-3">
                  <div class="service_wrapper_icon mb-4 d-inline-block rounded-circle d-flex align-items-center justify-content-center text-white">
                    <i data-feather="paperclip"></i>
                  </div>
                  <h3 class="mb-3">Printdesign</h3>
                  <p>Lorem ipsum dolor sit ame nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section class="work container py-5">
        <h3 class="pre_headline" id="work">Bisherige Arbeiten</h3>
        <h2>Referenzen</h2>
        <p class="col-lg-5">Eine kleine Übersicht von Projekten, an denen ich bisher gearbeitet habe. Auf meinem <a href="#">Instagram-Kanal</a> findest du zusätzlich weitere Einblicke in meine Arbeit als Designer.</p>
        <div class="work_item row align-items-center justify-content-between px-0 py-5">
          <div class="col-lg-5">
            <img src="./assets/images/godqScreen.png" alt="GodQ eSports" width="400px" class="img-fluid">
          </div>
          <div class="col-lg-5 text">
            <h3 class="mb-0">GodQ eSports</h3>
            <small class="d-block mb-3">Web- und Mediendesign</small>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit </p>
            <button class="btn-border shadow-none">Mehr erfahren</button>
          </div>
        </div>
        <div class="work_item row align-items-center justify-content-between px-0 py-5">
          <div class="col-lg-5 text">
            <h3 class="mb-0">Ein Chor</h3>
            <small class="d-block mb-3">Webdesign, WordPress Entwicklung</small>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit </p>
            <button class="btn-border shadow-none">Mehr erfahren</button>
          </div>
          <div class="col-lg-5">
            <img src="./assets/images/shalomchorScreen.png" alt="Shalomchor Neuenrade" width="400px" class="img-fluid">
          </div>
        </div>
        <div class="work_item row align-items-center justify-content-between px-0 py-5">
          <div class="col-lg-5">
            <img src="./assets/images/shalomchorScreen.png" alt="Shalomchor Neuenrade" width="400px" class="img-fluid">
          </div>
          <div class="col-lg-5 text">
            <h3 class="mb-0">Personal Branding</h3>
            <small class="d-block mb-3">Webdesign, Print- und Mediendesign</small>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit </p>
            <button class="btn-border shadow-none">Mehr erfahren</button>
          </div>
        </div>
      </section>
      <section class="contact container py-5">
        <div class="bg-red-orange d-flex justify-content-center align-items-center py-5 rounded mb-5">
          <h2 class="text-white w-75 lh-base my-3">Interessiert an einer Zusammenarbeit, um gemeinsam etwas Großartiges zu erschaffen? Schreib mir eine E-Mail und erzähl mir von deinen ersten Ideen.</h2>
        </div>
        <div class="row">
          <div class="col-lg-5">
            <h2 id="contact">Kontakt</h2>
            <p class="col-xl-10">Schreibe alternativ eine E-Mail an <a href="mailto:info@davidleven.de">info@meinemail.de</a>, schick mir eine Nachricht über <a href="https://instagram.com/dvdleven" title="Mein Instagram">Instagram</a> oder sende mir eine Anfrage bei <a href="javascript:void(0)" id="discord-tooltip" data-bs-toggle="popover" title="🤝 Discord-Tag:" data-bs-content="Nighty#8248">Discord</a>.</p>
            <img class="img-fluid mt-4" width="300px" src="./assets/images/conversation.svg">
          </div>
          <div class="col-lg-6 ms-auto bg-white rounded shadow-sm p-5">
            <form class="row g-3">
              <div class="col-md-6">
                <label for="nameInput" class="form-label">Dein Name:</label>
                <input type="text" class="form-control" id="nameInput">
              </div>
              <div class="col-md-6">
                <label for="emailInput" class="form-label">Deine E-Mail Adresse:</label>
                <input type="text" class="form-control" id="emailInput">
              </div>
              <div class="col-12">
                <label for="subjectInput" class="form-label">Betreff</label>
                <select id="subjectInput" class="form-select">
                  <option selected>Bitte auswählen...</option>
                  <option>Allgemeine Frage</option>
                  <option>Interesse an einer Zusammenarbeit</option>
                  <option>Sonstiges</option>
                </select>
              </div>
              <div class="col-12">
                <label for="inputMessage" class="form-label">Deine Nachricht:</label>
                <textarea class="form-control" id="inputMessage" rows="4"></textarea>
              </div>
              <div class="col-12">
                <div class="form-check">
                  <input class="form-check-input" type="checkbox" id="gridCheck">
                  <label class="form-check-label" for="gridCheck">
                    Ich bin mit der Speicherung meiner Daten zur Bearbeitung der Anfrage einverstanden.
                  </label>
                </div>
              </div>
              <div class="col-12">
                <button type="submit" class="btn-bg-purple">Abschicken</button>
              </div>
            </form>
          </div>
        </div>
      </section>
    </main>
    <?php include "../site_elements/footer.php"; ?>
  </body>
</html>
